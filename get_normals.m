function [normals] = get_normals(faces, v)

Nvert = length(v);
Nface = length(faces);
normals = zeros(Nvert,3);
v1=faces(:,1); v2=faces(:,2); v3=faces(:,3);
r1=v(v1,:); r2=v(v2,:); r3=v(v3,:);
vec1=r2-r1; vec2=r3-r1;
vec=.5*cross(vec1,vec2);

for j=1:Nface
    normals(v1(j),:) = normals(v1(j),:)+vec(j,:);
    normals(v2(j),:) = normals(v2(j),:)+vec(j,:);
    normals(v3(j),:) = normals(v3(j),:)+vec(j,:);
end;
% size(vec)
% size(normals)
% normals = vec;

dnor=sqrt(dot(normals',normals')');
normals(:,1)=normals(:,1)./dnor;
normals(:,2)=normals(:,2)./dnor;
normals(:,3)=normals(:,3)./dnor;

% figure;
% trimesh(faces,v(:,1),v(:,2),v(:,3),'Facecolor','red','EdgeColor','black','LineWidth', 1, 'FaceAlpha', 0.9); lighting phong;
% hold on;
% xnor = zeros(2, 3);
% for i=1:size(v, 1)
%     xnor(1,:) = v(i,:);
%     xnor(2,:) = v(i,:) + normals(i,:);
%     plot3(xnor(:,1), xnor(:,2), xnor(:,3));
% end
