function [ faces, vertices, normals, markvertices, markfaces, NVBoundCircles ] = CableMesh( L, NL, R, NR, r, n, k )
%   This function generates the Cable mesh
%   Input:
%   L  - length of cable
%   NL - discretization of L of cable
%   R  - radius of cable (outer disk)
%   NR - the number of circles on the annular disk
%   r  - radius of the inner conductor (inner disk)
%   n  - the number of vertices on the outer circumference of the cable
%   k  - the number of vertices on the inner circumference of the cable
%   Outup:
%   faces          - all faces of cable
%   vertices       - all vertices of cable mesh
%   markvertices   - 
%   markfaces      -  
%   NVBoundCircles - в этом массиве храняться номера вершин уложенные в
%   таком порядке: 1. Indices of vertices outer circumference (принятого left сечения или нижнего сечения)
%                  2. Indices of vertices inner circumference (принятого left сечения или нижнего сечения)
%                  3. Indices of vertices outer circumference (принятого right сечения или верхнего сечения)
%                  4. Indices of vertices inner circumference (принятого right сеченияи или верхнего сечения)
%               Плохо написал! Надо исправить!
%   запускать: CableMesh(160, 50, 10, 5, 5, 50, 25);

NVBoundCircles = [(1:n)'; (NL*n+1:NL*n+k)'; ((NL-1)*n+1:NL*n)'; (NL*n+(NL-1)*k+1:NL*n+NL*k)']; % переписать!

alpha = linspace(0, 2*pi, n+1); %0 degree equals 180 degress -> n+1
alpha = alpha(1:end-1)'; % разбиваем окружность на n частей. (углы, углы, углы)
%строим окружность
x = R*sin(alpha); 
y = R*cos(alpha);
%вытягиваем её по оси Z
z = linspace(0, L, NL);
Zn = length(z); % количество слоев по оси Z; здесь Zn = NL?
Zn
NL
vertices = zeros(n*Zn + k*Zn, 3); % готовим массив под наши вершины (зачем? думаешь так будет быстрее? это не тот случай)
% n*Zn - вершины поверхности кабеля (без боковых дисков)
% k*Zn - вершины поверхности внутреннего провода кабеля

tmp_z = z(ones(1,n),:); % также можно через repmat(z,k,1);
tmp_z = tmp_z(:);       % вытягиваем в вектор
vertices(1:n*Zn,:) = [ repmat([x y], NL, 1) tmp_z ];

%==============  МАРКИРОВКА  ======================
markvertices = zeros(n*Zn, 1);
markvertices = [markvertices; 3*ones(k*Zn, 1)];
%==================================================

% То же самое, только для внутреннего цилиндра
alpha2 = linspace(0, 2*pi, k+1); % 0 degree equals 180 degress => k+1
alpha2 = alpha2(1:end-1)'; % разбиваем окружность на k частей. (углы, углы, углы)
%строим окружность
x = r*sin(alpha2); 
y = r*cos(alpha2);

tmp_z = z(ones(1,k),:);
tmp_z = tmp_z(:);
vertices(Zn*n+1:end,:) = [ repmat([x y], NL, 1) tmp_z ];


% faces = zeros( (Zn-1)*n*2+(Zn-1)*k*2, 3 );
% структура такова: боковая поверхность цилиндра -> боковая поверхность
% внутреннего цилиндра -> нижний диск -> верхний диск
% (Zn-1)*n*2 - кол-во треугольников на поверхности кабеля (без дисков)
% (Zn-1)*k*2 - кол-во треугольников на поверхности внутреннего провода (без дисков)
%==============  МАРКИРОВКА  ======================
markfaces = zeros((Zn-1)*n*2, 1);
markfaces = [markfaces; 3*ones((Zn-1)*k*2, 1)];
%==================================================

% обозначаем грани для треугольников боковой поверхности внешнего цилиндра
%--------------------------------------------------------------------------
ind = 1:n-1;
M = repmat(ind, Zn-1, 1);
nv = 0:n:(Zn-2)*n;
A = M + repmat(nv', 1, n-1);   %вершины с номером i
A = A';
B = A + 1;                     %вершины с номером i + 1 
C = (A + n);                   %вершины с номером i + n
D = (C + 1);                   %вершины с номером i + n + 1
faces = [    B(:)      A(:)      C(:);
             C(:)      D(:)      B(:);
             B(end,:)' A(1,:)'   D(end,:)'
             D(end,:)' C(1,:)'   A(1,:)'];
%--------------------------------------------------------------------------

% %===============================================
% faces(2*(NL-1)*(n-1)+1,:)
% normals = get_normals([faces(1:n-1,:); faces(2*(NL-1)*(n-1)+1,:)], vertices(1:2*n,:));
% size(normals)
% normals = repmat(-normals(1:size(normals, 1)/2,:), NL, 1);
% size(normals)
% %===============================================

ind = 1:k-1; %генерируем индексы вершин первого слоя внутреннего цилиндра
ind = ind + Zn*n;
M = repmat(ind, Zn-1, 1);
nv = 0:k:(Zn-2)*k;
% size(M)
% size()
A = M + repmat(nv', 1, k-1);   %вершины с номером i
A = A';
B = A + 1;                     %вершины с номером i + 1 
C = (A + k);                   %вершины с номером i + k
D = (C + 1);                   %вершины с номером i + n + 1
faces = [              faces;
              A(:)      B(:)      C(:);
              D(:)      C(:)      B(:);
             B(end,:)' A(1,:)'   D(end,:)'
             D(end,:)' C(1,:)'   A(1,:)'];
%--------------------------------------------------------------------------
% %===============================================
% faces((Zn-1)*n*2+1,:) - NL*n
% tmp_normals = get_normals([faces((Zn-1)*n*2+1:(Zn-1)*n*2+k-1,:) - NL*n; faces((Zn-1)*n*2+2*(NL-1)*(k-1)+1,:) - NL*n], vertices(NL*n+1:NL*n+2*k,:));
% % size(normals)
% % size(repmat(tmp_normals(1:size(tmp_normals, 1)/2), NL, 1))
% normals = [normals; repmat(tmp_normals(1:size(tmp_normals, 1)/2,:), NL, 1)];
% %===============================================

% Генерация нижнего диска и сцепка последнего к кабелю.
%---------------------------------------------------
[f, v] = Disk( R, n, r, k, NR, vertices(1:n,:) ); % фишка, которую надо здесь применить -- f можно дублировать со смещением и для верхнего диска
tmp_f_column = f(:,1);
f(:,1) = f(:,2);
f(:,2) = tmp_f_column;
% %=============================== ОТРЕФАКТОРИТЬ! ====================
% tmp_normals = get_normals(f, v);
% normals = [normals; -tmp_normals(n+k+1:end,:)];
% normals(1:n,:) = normals(1:n,:) -tmp_normals(1:n,:);
% normals(NL*n+1:NL*n+k,:) = normals(NL*n+1:NL*n+k,:) -tmp_normals(n+1:n+k,:);
% %=============================== ОТРЕФАКТОРИТЬ! ====================
% % normals(NL*n+1:n,:) = normals(1:n,:) + tmp_normals(1:n,:);
vertices = [vertices; v(k+n+1:end, :)];
markvertices = [markvertices; 1*ones(size(v,1)-n-k, 1)];
% tic;
indexes = find( f > n+k );
f(indexes) = f(indexes) + (n*Zn+k*Zn-n-k);
indexes = find( f > n  & f <= n+k );
f(indexes) = f(indexes) + (n*Zn-n);
faces = [faces; f];
% find_time = toc;
markfaces = [markfaces; 1*ones(size(f, 1), 1)];
%----------------------------------------------------

% Генерация верхнего диска и сцепка последнего к кабелю.
%---------------------------------------------------
[f, v] = Disk( R, n, r, k, NR, vertices((Zn-1)*n+1:Zn*n,:) );% (Zn-1)*n+1:Zn*n - вершины последнего кольца внешней боковой поверхности цилиндра
% %=============================== ОТРЕФАКТОРИТЬ! ====================
% tmp_normals = get_normals(f, v);
% %=============================== ОТРЕФАКТОРИТЬ! ====================
% normals = [normals; tmp_normals(n+k+1:end,:)];
% normals((NL-1)*n+1:NL*n,:) = normals((NL-1)*n+1:NL*n,:) + tmp_normals(1:n,:);
% normals(NL*n+(NL-1)*k+1:NL*n+(NL)*k,:) = normals(NL*n+(NL-1)*k+1:NL*n+(NL)*k,:)  + tmp_normals(n+1:n+k,:);
% %=============================== ОТРЕФАКТОРИТЬ! ====================
size_vert = size(vertices, 1);
vertices = [vertices; v(k+n+1:end, :)];
markvertices = [markvertices; 2*ones(size(v,1)-n-k, 1)];
% tic;
indexes = find( f > n+k );
f(indexes) = f(indexes) + ( size_vert-n-k );
indexes = find( f <= n);
f(indexes) = f(indexes) + ( (NL-1)*n );
indexes = find( f > n  & f <= n+k );
f(indexes) = f(indexes) + (n*Zn+(NL-1)*k-n);
faces = [faces; f];
% find_time = toc;
markfaces = [markfaces; 2*ones(size(f, 1), 1)];
%----------------------------------------------------

% figure();
% trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);

normals = get_normals(faces, vertices);


end

