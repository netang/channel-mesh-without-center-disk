n = 50;
k = 25;
[ faces, vertices, normals, markvertices, markfaces, NVBoundCircles ] = CableMesh(10, 10, 10, 5, 5, n, k);
indv_cont_in_0 = NVBoundCircles(1:n);
indv_cont_in_3 = NVBoundCircles(n+1:n+k);
indv_cont0 = NVBoundCircles(n+k+1:n+k+n); 
indv_cont3 = NVBoundCircles(n+k+n+1:end); % Если ты не описал структуру NVBoundCicles, то это плохой код!


figure();
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);

figure();
trisurf(faces(markfaces == 0, :), vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
title('markfaces = 0');
figure();
trisurf(faces(markfaces == 1, :), vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
title('markfaces = 1');
figure();
trisurf(faces(markfaces == 2, :), vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
title('markfaces = 2');
figure();
trisurf(faces(markfaces == 3, :), vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
title('markfaces = 3');

vertices(markvertices == 2, 3) = vertices(markvertices == 2, 3) + 5; % проверка markevertives'ов ( 2 и 3 )
vertices(markvertices == 1, 3) = vertices(markvertices == 1, 3) - 5;
figure();
trisurf(faces, vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.5);
title('markvertices 2 and 3 changed (Good looking!)');
axis equal;

vertices(markvertices == 2, 3) = vertices(markvertices == 2, 3) - 5;
vertices(markvertices == 1, 3) = vertices(markvertices == 1, 3) + 5;
figure;
% trimesh(faces,vertices(:,1), vertices(:,2), vertices(:,3),'Facecolor','red','EdgeColor','black','LineWidth', 1, 'FaceAlpha', 0.9); lighting phong;
trimesh(faces,vertices(:,1), vertices(:,2), vertices(:,3), 'FaceAlpha', 0.8);
hold on;
xnor = zeros(2, 3);
for i=1:size(vertices, 1)
    xnor(1,:) = vertices(i,:);
    xnor(2,:) = vertices(i,:) + normals(i,:);
    plot3(xnor(:,1), xnor(:,2), xnor(:,3), 'LineWidth', 2);
end
title('normals');
hold on;
% for i=1:length(indv_cont_in_0)
%     text(vertices(indv_cont_in_0(i),1), vertices(indv_cont_in_0(i),2), vertices(indv_cont_in_0(i),3), ...
%         num2str(indv_cont_in_0(i)));
% end;
% for i=1:length(indv_cont_in_3)
%     text(vertices(indv_cont_in_3(i),1), vertices(indv_cont_in_3(i),2), vertices(indv_cont_in_3(i),3), ...
%         num2str(indv_cont_in_3(i)));
% end;
% for i=1:length(indv_cont0)
%     text(vertices(indv_cont0(i),1), vertices(indv_cont0(i),2), vertices(indv_cont0(i),3), ...
%         num2str(indv_cont0(i)));
% end;
for i=1:length(indv_cont3)
    text(vertices(indv_cont3(i),1), vertices(indv_cont3(i),2), vertices(indv_cont3(i),3), ...
        num2str(indv_cont3(i)));
end;
axis equal;